var botao = document.querySelector(".btn1");
var btnresultado = document.querySelector(".btn2")
var campomedia = document.querySelector("#resultado");
var elementolista = document.querySelector(".lista");
var notas = []
var contador = 0
botao.addEventListener("click", () => {
    var notadigitada = document.querySelector("#digitenota").value;
    if (notadigitada == "") {
        alert("Por favor, insira uma nota")
    }else{
        notadigitada = notadigitada.toString();
        notadigitada = parseFloat(notadigitada.replace(",", "."));
        if (0 <= notadigitada && notadigitada <= 10) {
            notas.push(notadigitada)
            contador++            
            var num = ("a nota " + contador + " foi: " + notadigitada)
            digitenota.value = "";            
            var elementonota = document.createElement("li");
            elementonota.innerHTML = `<span>${num}</span>`
            elementolista.appendChild(elementonota)
        }
        else if (0 < notadigitada || notadigitada > 10) {
            alert("A nota digitada é inválida, por favor, insira uma nota válida.")
            digitenota.value = "";
        }else{
            alert("A nota digitada é inválida, por favor, insira uma nota válida.")
            digitenota.value = "";
        }
    } 

})

btnresultado.addEventListener("click", () => {
    var soma = 0;
    for (i = 0; i < notas.length; i++){
        var notasn = parseFloat(notas[i])
        soma += notasn
    }
    var media = soma / notas.length
    var mediafixxed = media.toFixed(2)
    var mediafin = document.createElement("span")
    mediafin.innerHTML = `<span>${mediafixxed}</span>`
    campomedia.innerText = "";
    campomedia.appendChild(mediafin)
    notas = []
    contador = 0;
    elementolista.innerText = "";
})